#ifndef _CAMERA_CPP
#define _CAMERA_CPP

#include "Ponto.h"
#include "Vetor.cpp"
#include <math.h>
#include <vector>

class Camera
{
private:
	Vetor cam_n;
	Vetor cam_v;
	Vetor cam_u;
	double cam_d;
	double cam_hx;
	double cam_hy;
	Vetor cam_c;

public:
	Camera() {}

	void setParametros(Vetor n, Vetor v, Vetor u, double d, double hx, double hy, Vetor c) {
		this->cam_n = n;
		this->cam_v = v;
		this->cam_u = u;
		this->cam_d = d;
		this->cam_hx = hx;
		this->cam_hy = hy;
		this->cam_c = c;
	}

	/*
	* M�todo que ir� retornar uma base ortonormal ao se passar dois vetores quaisquer.
	*/
	Vetor* gramSchmidt(Vetor v, Vetor n){
		double pEscalar = (v.produtoEscalar(n) / n.produtoEscalar(n)); 
		Vetor temp = n.escalar_x_Vetor(pEscalar);
		Vetor v1 = v -  temp;
		Vetor u = n.produtoVetorial(v1);
		Vetor *base = new Vetor[3];
		base[0] = u.normalizar();
		base[1] = v1.normalizar();
		base[2] = n.normalizar();
		return base;
	}

	/*
	* M�todo que passados tres vetores que formam uma base ortonormal do sistema de 
	* coordenadas de vista e um ponto no sistema de coordenadas mundiais como parametros,
	* calcula outro ponto no sistema de coordenadas de vista.
	*/
	Ponto mundiaisVista(Vetor* baseOrtonormal, Ponto c, Ponto p) 
	{
		Vetor u = baseOrtonormal[0];
		Vetor v = baseOrtonormal[1];
		Vetor n = baseOrtonormal[2];

		double x = u.getP().getX() * (p.getX() - c.getX()) + 
				   u.getP().getY() * (p.getY() - c.getY()) + 
				   u.getP().getZ() * (p.getZ() - c.getZ());
		
		double y = v.getP().getX() * (p.getX() - c.getX()) + 
				   v.getP().getY() * (p.getY() - c.getY()) + 
				   v.getP().getZ() * (p.getZ() - c.getZ());
		
		double z = n.getP().getX() * (p.getX() - c.getX()) + 
				   n.getP().getY() * (p.getY() - c.getY()) + 
				   n.getP().getZ() * (p.getZ() - c.getZ());

		Ponto vista(x, y, z);
		
		return vista;
	}

	/*
	* M�todo que calcula a proje��o em perspectiva de um ponto em coordenadas de vista.
	*/
	Ponto projecaoPerspectivaNormalizada(Ponto p, double d, double hx, double hy) 
	{
		double xs = (d*p.getX()) / (hx*p.getZ());
		double ys = (d*p.getY()) / (hy*p.getZ());
		Ponto projecao(xs, ys, d);
		return projecao;
	}

	Ponto vistaTela(Ponto vista, double resX, double resY) 
	{
		double x_tela =  floor(((vista.getX() + 1)/2)* resX + 0.5);
		double y_tela =  floor(resY - (((vista.getY() + 1)/2 )* resY) + 0.5);
		Ponto pontoTela(x_tela,y_tela,0);
		
		return pontoTela;		
	}

	~Camera() {}

}
;

#endif