#ifndef _Ponto_CPP
#define _Ponto_CPP

#include <allegro.h>

class Ponto {

private:
	double x;
	double y;
	double z;

public:
	Ponto() {}
	Ponto (double x, double y, double z);
	double getX ();
	double getY ();
	double getZ ();
	void setX (double x);
	void setY (double y);
	void setZ (double z);
	void desenhaLinha (BITMAP *fundo, Ponto *pontoFinal, double cor);
	double* calcula_equacao_reta (Ponto *pontoFinal);

	void calcula_novo_ponto (Ponto *pontoNovo, Ponto *pontoFinal, double incremento);
	void setValues (double x, double y, double z);
	void setValues (Ponto p);
	  
};
  
#endif