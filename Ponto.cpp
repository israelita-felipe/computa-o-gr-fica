#include "Ponto.h"

Ponto::Ponto(double x, double y, double z) {
	this->x = x;
	this->y = y;
	this->z = z;
}

double Ponto::getX () {
	return Ponto::x;
}

double Ponto::getY () {
	return Ponto::y;
}


double Ponto::getZ () {
	return Ponto::z;
}

void Ponto::setX (double x) {
	this->x = x;
}

void Ponto::setY (double y) {
	this->y = y;
}

void Ponto::setZ (double z) {
	this->z = z;
}

/* Calcula a equa��o da Reta
   Dados dois Pontos*/

double* Ponto::calcula_equacao_reta (Ponto *pontoFinal) {
	double *a_b = new double[2];

	double X1 = this->x;
	double Y1 = this->y;
	double X2 = pontoFinal->x;
	double Y2 = pontoFinal->y;

	double a = (double)Y1-Y2;
	a = a / (X1-X2);

	double b = Y1 - (a*X1);

	a_b[0] = a;
	a_b[1] = b;
	return a_b;
}

/* M�todo que calcula dois Pontos paralelos 
   Contidos em dois lados opostos de um triangulo
   dado o incremento do eixo Y a partir do ponto mais
   acima*/

void Ponto::calcula_novo_ponto (Ponto *pontoNovo, Ponto *pontoFinal, double incremento) {
	pontoNovo->setY(this->y + incremento);
	if (this->x != pontoFinal->x) {
		double *a_b;
		a_b = new double [2];
		a_b = this->calcula_equacao_reta(pontoFinal);
		double X =  (pontoNovo->getY() - a_b[1]);
		X =  (double)( (double) (X / a_b[0]) + 0.5);
		pontoNovo->setX(X);
		delete []a_b;
	} else {
		pontoNovo->setX(this->x);
	}
}

/* M�todo que desenha uma linha
   dados os pontos de origem e 
   destino utilizando apenas
   PutPixel*/

void Ponto::desenhaLinha(BITMAP *fundo, Ponto *pontoFinal, double cor){
    double dx =  (pontoFinal->getX() - this->getX());
    double dy =  (pontoFinal->getY() - this->getY());

    double x1,x2,y1,y2;
    x1 = this->getX();
    y1 = this->getY();
    x2 = pontoFinal->getX();
    y2 = pontoFinal->getY();

	float t = 0.5;
	putpixel(fundo, x1, y1, cor);
	
	if (abs((int)dx*10000) > abs((int)dy*10000)) {
	    float a = (float) dy / (float) dx;
		t = t + y1;
		dx = (dx < 0) ? -1 : 1;
		a = a * dx;
             
		while (x1 != x2) {
			x1 = (x1 + dx);
			t = t + a;
			putpixel(fundo, x1, t, cor);		
   	   	}
	
	} else {
		if ( dy != 0 ) {
			float a = (float) dx / (float) dy;
			t = t + x1;
			dy = (dy < 0) ? -1 : 1;
			a = a * dy;
					
			while ( y1 != y2) {
				y1 = (y1 + dy);
				t = t + a;
				putpixel(fundo, t, y1, cor);		
			 }
		}
	}

}

/*
* M�todo que seta os tres valores dos eixos do ponto.
*/
void Ponto::setValues (double x, double y, double z){
		this->x = x;
		this->y = y;
		this->z = z;
};

void Ponto::setValues (Ponto p){
		this->x = p.getX();
		this->y = p.getY();
		this->z = p.getZ();
};
