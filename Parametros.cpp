#ifndef _PARAMETROS_CPP
#define _PARAMETROS_CPP

#include "Cor.cpp"
#include "Ponto.h"
#include "Vetor.cpp"
#include "Intensidade.cpp"



class Parametros {

	
	private:


	public:
	Vetor n;
	Vetor v;
	Vetor u;

	double d;
	double hx;
	double hy;

	Vetor c;
	Cor iamb;
	double ka;

	Cor il;
	Ponto pl;
	Intensidade kd;

	Intensidade od;
	double ks;
	double eta;


	Parametros(Vetor n,Vetor v,	Vetor u, double d, double hx,
				double hy,Vetor c,Cor iamb, double ka, Cor il,
				Ponto pl, Intensidade kd, Intensidade od, double ks, double eta){
	
		this->n = n;
		this->v = v;
		this->u = u;
		this->d = d;
		this->hx = hx;
		this->hy = hy;
		this->c = c;
		this->iamb = iamb;
		this->ka = ka;
		this->il = il;
		this->pl= pl;
		this->kd = kd;
		this->od = od;
		this->ks = ks;
		this->eta = eta;
	
	
	}

	
};
#endif