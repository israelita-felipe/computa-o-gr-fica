#ifndef _COR_CPP
#define _COR_CPP

class Cor
{
private: 
	double r;
	double g;
	double b;

public:
	Cor(){
	}

	Cor (double r, double g, double b)
	{
		this->r = r;
		this->g = g;
		this->b = b;
	}

	double getR() {
		return this->r;
	}

	double getG() {
		return this->g;
	}

	double getB() {
		return this->b;
	}

	~Cor (){}
};

#endif