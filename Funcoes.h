#ifndef _FUNCOES_CPP
#define _FUNCOES_CPP

#include <vector>
#include <math.h>
#include "Ponto.h"
#include "Cor.cpp"
#include "Intensidade.cpp"
#include "Vetor.cpp"
#include "Triangulo.h"

class Triangulo;

class Funcoes
{
private:
	
	void putPixel(int x, int y, Triangulo* triangulo, BITMAP* buffer, Ponto* P1_tela,
	Ponto* P2_tela, Ponto* P3_tela,	Ponto* P1_vista, Ponto* P2_vista, Ponto* P3_vista, 
	Vetor* normal_vertice1, Vetor* normal_vertice2, Vetor* normal_vertice3);


public:

	std::vector <Triangulo> lista_de_triangulos;
	std::vector <Vetor> lista_de_normais_triangulos;
	std::vector <Vetor> lista_de_normais_vertices;
	std::vector <Ponto> lista_de_pontos_tela;
	std::vector <Ponto> lista_de_pontos_vista;
	double **zBuffer;
	

	// parametros lidos do arquivo
	Vetor n;
	Vetor v;
	Vetor u;

	double d;
	double hx;
	double hy;

	Vetor c;
	Cor iamb;
	double ka;

	Cor il;
	Ponto pl;
	Intensidade kd;

	Intensidade od;
	double ks;
	double eta;
	char objeto[20];


	Funcoes() {}
	~Funcoes();
	void lerArquivo();
	void lerParametros();
	void mundoVista () ;
	void vistaPerspectiva ();
	void imprimirTriangulos(BITMAP* buffer);
	void perspectivaTela ();
	Ponto pontoMundoVista (Ponto p);
	void calcularNormaisTriangulos();
	void calcularNormaisVertices();
	Vetor* getVertices(Triangulo t);
	Vetor* getVertices(double indice1, double indice2, double indice3);
	Vetor calcularNormalTriangulo(Triangulo t);
	void Funcoes::ordenarTriangulosBaricentro();
	double distanciaDoisPontos(Ponto p1, Ponto p2);
	void scanLine (BITMAP *buffer, Triangulo* t, int indice_triangulo_vector);
	void algoritmo_zBuffer(Ponto* Origem, Ponto* Destino, BITMAP *buffer, 
								int indice_triangulo_vector, Triangulo* triangulo,
								Ponto* P1_tela, Ponto* P2_tela,	Ponto* P3_tela,
								Ponto* P1_vista, Ponto* P2_vista, Ponto* P3_vista,
								Vetor* normal_vertice1,	Vetor* normal_vertice2,
								Vetor *normal_vertice3);


//=========================================================================
//PHONG
Cor Funcoes::calcCompAmbiental();

Intensidade Funcoes::calcCompDifusa(Ponto pVarrido, Vetor n);

Intensidade Funcoes::calcCompEspecular(Ponto pVarrido, Vetor n) ;

Cor Funcoes::calcularCor(Ponto pVarrido, Vetor n, Triangulo t) ;

};


#endif