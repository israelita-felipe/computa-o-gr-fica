#include <allegro.h>
#include <stdio.h>
#include "Triangulo.h"
#include "Funcoes.h"
#include <vector>
#include "Camera.cpp"

int main() 
{      
 	allegro_init();      
	set_color_depth(24);
	set_gfx_mode(GFX_AUTODETECT, 800, 600, 0, 0);
	install_keyboard();
	install_mouse();
	clear_to_color(screen,16777215);
	show_mouse(screen);

	BITMAP *buffer;
	BITMAP *botao;
	botao = load_bitmap("botao.bmp", NULL);
	buffer = create_bitmap(800,600);
	Funcoes *f = new Funcoes();
	
//=============================================================================================
inicio:

	clear_to_color(buffer,16777215);
	f->lerArquivo();
	f->imprimirTriangulos(buffer);
	draw_sprite(buffer, botao, 722, 566);
	show_mouse(NULL);
	blit(buffer, screen, 0, 0, 0, 0, 800, 600);	
	show_mouse(screen);
 
	while(!key[KEY_ESC]){
		
		if(key[KEY_A] || ( (mouse_x > 722 && mouse_y > 566) &&  mouse_b & 1) ) {
			show_mouse(NULL);
			clear(buffer);
			clear(screen);
			f->~Funcoes();			
			show_mouse(screen);
			goto inicio;
		}		
	}
	
 	allegro_exit();
	return 0;
}
END_OF_MAIN();

