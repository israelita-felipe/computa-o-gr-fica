#include "Triangulo.h"
#include "Cor.cpp"
#include "Intensidade.cpp"

Triangulo::Triangulo (int indice1, int indice2, int indice3) {
	this->indice1 = indice1;
	this->indice2 = indice2;
	this->indice3 = indice3;	
}

Triangulo::Triangulo () {
}


/* Ordena os Pontos do Triangulo
   Para o Algorítimo scanline */
void Triangulo::ordenarPontos (Ponto* P1, Ponto* P2, Ponto* P3) {

    int a, b, c;
    double Y1, Y2, Y3;
    Y1 = P1->getY();
    Y2 = P2->getY();
    Y3 = P3->getY();    
    a = Y1<=Y2;
    b = Y1<=Y3;
    c = Y2<=Y3;
    
    Ponto *temp = new Ponto();
    
    if (a == 0 && b == 0 && c == 0) {
		*temp = *P1;   
		*P1 = *P3;    
		*P3 = *temp;
    }
    if (a == 0 && b == 0 && c == 1) {
        *temp = *P1;
		*P1 = *P2;
        *P2 = *P3;
	   	*P3 = *temp;
	}
	if (a ==0 && b == 1 && c == 1) {
		*temp = *P2;
		*P2 = *P1;
		*P1 = *temp;    		
    }
    if (a == 1 && b == 0 && c == 0) {
		*temp = *P1;
		*P1 = *P3;
		*P3 = *P2;
		*P2 = *temp;    
    } 
    if (a == 1 && b == 1 && c == 0) {
		*temp = *P3;
		*P3 = *P2;
		*P2 = *temp;   		
    } 
	delete temp;
}

void Triangulo::ordenarPontos (Ponto P1, Ponto P2, Ponto P3, int* indice1, int* indice2, int* indice3) {

    int a, b, c;
    double Y1, Y2, Y3;
    Y1 = P1.getY();
    Y2 = P2.getY();
    Y3 = P3.getY();    
    a = Y1<=Y2;
    b = Y1<=Y3;
    c = Y2<=Y3;
    
    int *temp = new int;
    
    if (a == 0 && b == 0 && c == 0) {
		*temp = *indice1;   
		*indice1 = *indice3;    
		*indice3 = *temp;
    }
    if (a == 0 && b == 0 && c == 1) {
        *temp = *indice1;
		*indice1 = *indice2;
        *indice2 = *indice3;
	   	*indice3 = *temp;
	}
	if (a ==0 && b == 1 && c == 1) {
		*temp = *indice2;
		*indice2 = *indice1;
		*indice1 = *temp;    		
    }
    if (a == 1 && b == 0 && c == 0) {
		*temp = *indice1;
		*indice1 = *indice3;
		*indice3 = *indice2;
		*indice2 = *temp;    
    } 
    if (a == 1 && b == 1 && c == 0) {
		*temp = *indice3;
		*indice3 = *indice2;
		*indice2 = *temp;   		
    } 
	delete temp;
}

void Triangulo::ordenarPontos (Ponto* P1, Ponto* P2, Ponto* P3, 
							   Ponto* P1_vista, Ponto* P2_vista, Ponto* P3_vista,
							   Vetor* V1, Vetor* V2, Vetor* V3) {

    int a, b, c;
    double Y1, Y2, Y3;
    Y1 = P1->getY();
    Y2 = P2->getY();
    Y3 = P3->getY();    
    a = Y1<=Y2;
    b = Y1<=Y3;
    c = Y2<=Y3;
    
    Ponto *temp = new Ponto();
	Ponto *temp2 = new Ponto();
	Vetor *temp3 = new Vetor();
    
    if (a == 0 && b == 0 && c == 0) {
		*temp = *P1;   
		*P1 = *P3;    
		*P3 = *temp;

		*temp2 = *P1_vista;   
		*P1_vista = *P3_vista;    
		*P3_vista = *temp2;

		*temp3 = *V1;   
		*V1 = *V3;    
		*V3 = *temp3;
    }
    if (a == 0 && b == 0 && c == 1) {
        *temp = *P1;
		*P1 = *P2;
        *P2 = *P3;
	   	*P3 = *temp;

		*temp2 = *P1_vista;
		*P1_vista = *P2_vista;
        *P2_vista = *P3_vista;
	   	*P3_vista = *temp2;

		*temp3 = *V1;
		*V1 = *V2;
        *V2 = *V3;
	   	*V3 = *temp3;
	}
	if (a ==0 && b == 1 && c == 1) {
		*temp = *P2;
		*P2 = *P1;
		*P1 = *temp;    		

		*temp2 = *P2_vista;
		*P2_vista = *P1_vista;
		*P1_vista = *temp2;    		

		*temp3 = *V2;
		*V2 = *V1;
		*V1 = *temp3;    		
    }
    if (a == 1 && b == 0 && c == 0) {
		*temp = *P1;
		*P1 = *P3;
		*P3 = *P2;
		*P2 = *temp;    

		*temp2 = *P1_vista;
		*P1_vista = *P3_vista;
		*P3_vista = *P2_vista;
		*P2_vista = *temp2;    

		*temp3 = *V1;
		*V1 = *V3;
		*V3 = *V2;
		*V2 = *temp3;    
    } 
    if (a == 1 && b == 1 && c == 0) {
		*temp = *P3;
		*P3 = *P2;
		*P2 = *temp;   		

		*temp2 = *P3_vista;
		*P3_vista = *P2_vista;
		*P2_vista = *temp2;   		

		*temp3 = *V3;
		*V3 = *V2;
		*V2 = *temp3;   		
    } 
	delete temp;
	delete temp2;
	delete temp3;
}




//---------------------------------------------------------------------------------------------
double* Triangulo::calcularAlfaBetaGama (double x, double y, Ponto p, Ponto p2, Ponto p3) 
{
	double x1 = p.getX();
	double y1 = p.getY();

	double x2 = p2.getX();
	double y2 = p2.getY();

	double x3 = p3.getX();
	double y3 = p3.getY();

	
	double dalfa = this->determinante(x, x2, x3, y, y2, y3);
	double dbeta = this->determinante(x1, x, x3, y1, y, y3);
	double dgama = this->determinante(x1, x2, x, y1, y2, y);
	double d = this->determinante(x1, x2, x3, y1, y2, y3);

	double *alfaBetaGama = new double[3];

	alfaBetaGama[0] = (double)(((double) dalfa) / ((double) d));
	alfaBetaGama[1] = (double)(((double) dbeta) / ((double) d));
	alfaBetaGama[2] = (double)(((double) dgama) / ((double) d));

	if (d!= 0){
	return alfaBetaGama;
	} else {
	return NULL;
	}


}


double Triangulo::determinante(double x1, double x2, double x3, 
					double y1, double y2, double y3) 
{
	double d = ((x1 * y2) + (x2 * y3) + (x3 * y1)) - ((x3 * y2) + (x2 * y1) + (x1 * y3));
	return d;
}

Ponto Triangulo::coordZbaricentro(double* alfaBetaGama, Ponto p1, Ponto p2, Ponto p3){
	double alfa = alfaBetaGama[0];
	double beta = alfaBetaGama[1];
	double gama = alfaBetaGama[2];

	
	Vetor v1(p1);
	Vetor v2(p2);
	Vetor v3(p3);
	
	Vetor temp = v1.escalar_x_Vetor(alfa) + v2.escalar_x_Vetor(beta) + v3.escalar_x_Vetor(gama);
	
	return temp.getP();
}


void Triangulo::calcularBaricentro (std::vector <Ponto> Pontos) {

	
	Ponto *p1 = new Ponto();
	Ponto *p2 = new Ponto();
	Ponto *p3 = new Ponto();

	*p1 = Pontos[this->indice1];
	*p2 = Pontos[this->indice2];
	*p3 = Pontos[this->indice3];


	double z = p1->getZ() + p2->getZ() + p3->getZ();
	z = z/3;
	
	this->zBaricentro = z;
}


//---------------------------------------------------------------------------------------------



double Triangulo::getIndice(double indice)
	{
		if(indice == 0) 
		{
			return this->indice1;
		}			
		else if(indice == 1) 
		{
			return this->indice2;
		}			
		else if(indice == 2) 
		{
			return this->indice3;
		}
		else {
			return 0;
		}
	}



