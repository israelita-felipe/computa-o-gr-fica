#ifndef _TRIANGULO_CPP
#define _TRIANGULO_CPP

#include <allegro.h>
#include <vector>
#include <math.h>
#include "Ponto.h"
#include "Vetor.cpp"
#include "Funcoes.h"
#include "Parametros.cpp"

class Triangulo {

	private: 
	

	public:
		
		int indice1;
		int indice2;
		int indice3;

		int zBaricentro;

		Triangulo ();
		Triangulo (int indice1, int indice2, int indice3);
		
		void ordenarPontos (Ponto* P1, Ponto* P2, Ponto* P3);

		void Triangulo::ordenarPontos (Ponto P1, Ponto P2, Ponto P3, int* indice1, int* indice2, int* indice3);

		void ordenarPontos (Ponto* P1, Ponto* P2, Ponto* P3, 
							Ponto* P1_vista, Ponto* P2_vista, Ponto* P3_vista,
							Vetor* V1, Vetor* V2, Vetor* V3);

		void desenhaTriangulo (BITMAP *buffer, double **zBuffer, Parametros param);
		double* calcularAlfaBetaGama (double x, double y, Ponto p, Ponto p2, Ponto p3) ;
		double determinante(double x1, double x2, double x3, 
							double y1, double y2, double y3);
		Ponto coordZbaricentro(double* alfaBetaGama, Ponto p1, Ponto p2, Ponto p3);
		void calcularBaricentro (std::vector <Ponto> Pontos) ;
		double getIndice(double indice);
};


#endif