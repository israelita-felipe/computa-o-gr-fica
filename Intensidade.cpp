#ifndef _INTENSIDADE_CPP
#define _INTENSIDADE_CPP

class Intensidade 
{
private: 
	double ir;
	double ig;
	double ib;

public:
	Intensidade(){
	
	}
	Intensidade (double ir, double ig, double ib)
	{
		this->ir = ir;
		this->ig = ig;
		this->ib = ib;
	}

	double getIR() {
		return this->ir;
	}

	double getIG() {
		return this->ig;
	}

	double getIB() {
		return this->ib;
	}

	~Intensidade (){}
};

#endif