#ifndef _VETOR_CPP
#define _VETOR_CPP

#include <math.h>
#include "Ponto.h"

class Vetor 
{
private:
	Ponto p;

public:
	/*
	* Construtor padrao.
	*/
	Vetor () {}
	
	/*
	* Construtor que aceita um ponto como parametro e o outro ponto � a origem
	* do sistema, j� que um vetor � a subtra��o de dois pontos.
	*/
	Vetor (Ponto p) 
	{
		this->setP(p);
	}

	/*
	* M�todo que retorna o ponto final do vetor.
	*/
	Ponto getP()
	{
		return this->p;
	}

	/*
	* M�todo que seta o ponto final do vetor.
	*/
	void setP(Ponto p)
	{
		this->p.setX(p.getX());
		this->p.setY(p.getY());
		this->p.setZ(p.getZ());
	}

	/*
	* M�todo que d� overload no operador de soma (+) para que possa ser utilizado
	* na soma de dois vetores.
	*/
	Vetor operator+(Vetor v2)
	{
		Vetor v;
		double x = this->p.getX() + v2.getP().getX();
		double y = this->p.getY() + v2.getP().getY();
		double z = this->p.getZ() + v2.getP().getZ();
		Ponto p(x,y,z);
		v.setP(p);
		return v;
	}

	/*
	* M�todo que d� overload no operador de subtracao (-) para que possa ser utilizado
	* na subtracao de dois vetores.
	*/
	Vetor operator-(Vetor v2)
	{
		Vetor v;
		double x = this->p.getX() - v2.getP().getX();
		double y = this->p.getY() - v2.getP().getY();
		double z = this->p.getZ() - v2.getP().getZ();
		Ponto p(x,y,z);
		v.setP(p);
		return v;
	}
	
	/*
    * M�todo que retorna um vetor da subtra��o de dois pontos.
    * Primeiro menos o segundo ponto passado como par�metro.
    */
    static Vetor diffDoisPontos (Ponto p1, Ponto p2) 
    {
        Vetor v;
        double x = p1.getX() - p2.getX();
        double y = p1.getY() - p2.getY();
        double z = p1.getZ() - p2.getZ();
        Ponto p(x, y, z);
        v.setP(p);
        return v;
    }

	/*
	* M�todo que calcula a norma de um vetor.
	*/
	double norma () 
	{
		double norma2 = this->p.getX() * this->p.getX() + this->p.getY() * this->p.getY() + this->p.getZ() * this->p.getZ();
		double norma = (double) sqrt(norma2);
		return norma;
	}

	/*
	* M�todo que retorna o vetor normalizado.
	*/
	Vetor normalizar () 
	{
		double norma = this->norma();
		double x;
		double y;
		double z;
		if (norma != 0){
		x = this->p.getX()/norma;
		y = this->p.getY()/norma;
		z = this->p.getZ()/norma;
		}  else {
		x = 0;
		y = 0;
		z = 0;
		}
		Ponto p2(x, y, z);
		Vetor v(p2);
		return v;
	}

	/*
	* M�todo que calcula o produto vetorial entre dois vetores.
	*/
	Vetor produtoVetorial (Vetor v2) 
	{
		double x = this->p.getY() * v2.getP().getZ() - this->p.getZ() * v2.getP().getY();
		double y = -1 * (this->p.getX() * v2.getP().getZ() - this->p.getZ() * v2.getP().getX());
		double z = this->p.getX() * v2.getP().getY() - this->p.getY() * v2.getP().getX();
		Ponto p(x, y, z);
		Vetor v(p);
		return v;
	}

	/*
	* M�todo que calcula o produto escalar entre dois vetores..
	*/
	double produtoEscalar(Vetor v2)
	{
		double retorno = ((this->p.getX() * v2.getP().getX()) + 
						  (this->p.getY() * v2.getP().getY()) + 
						  (this->p.getZ() * v2.getP().getZ())); 
	
		return retorno;
	}
	
	/*
	* M�todo que calcula a multiplica��o de um vetor por um escalar.
	*/
	/*
	* M�todo que calcula a multiplica��o de um vetor por um escalar.
	*/
	Vetor escalar_x_Vetor(double escalar){
		
		Ponto p(this->p.getX() * escalar, this->p.getY() * escalar, this->p.getZ() * escalar);
		Vetor v(p);
		return v;
	}

	/*
	* Destrutor da classe.
	*/
	~Vetor () {}

};

#endif
