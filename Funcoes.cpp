#include "Funcoes.h"
#include "Triangulo.h"
#include <string.h>
#include "Camera.cpp"
#include <stdlib.h>
#include <math.h>



	/*
	* M�todo que cria e preenche uma lista de Pontos, uma lista de Triangulos,
	* e inicializa a lista de normais dos vertices.
	*/
void Funcoes::lerArquivo() 
{
   /*ler do arquivo 
   e cria os pontos
   */
	
	/*char *pasta = "./Objetos_BYU/";
	this->objeto = strcat (objeto , "");*/
	this->lerParametros();
	
	
	FILE *arquivo = fopen("./Objetos_BYU/maca.byu", "r");
	
	int num_pontos = 0;
	int num_triangulos = 0;
	fscanf(arquivo, "%d", &num_pontos);
	fscanf(arquivo, "%d", &num_triangulos);
	
	this->zBuffer = new double*[800];

 	for (int a = 0; a < 800; a++) {
		this->zBuffer[a] = new double[600];
	
	}


	for (int d = 0; d < 800; d++) {				
		for (int e = 0; e < 600; e ++) {
		this->zBuffer[d][e] = 999999999999;
		}
	}

	for(int i = 0; i < num_pontos; i++) 
	{
		double x = 0.0;
		double y = 0.0;
		double z = 0.0;

		fscanf(arquivo, "%lf", &x);
		fscanf(arquivo, "%lf", &y);
		fscanf(arquivo, "%lf", &z);

		Ponto p(x, y, z);
		this->lista_de_pontos_vista.push_back(p);
		
		
		Ponto normal_vertice_inicial(0, 0, 0);
		Vetor normal(normal_vertice_inicial);
		this->lista_de_normais_vertices.push_back(normal);
	}




		/* criar os Triangulos
	*/
		for(int j = 0; j < num_triangulos; j++) 
	{
		double indice_vertice1 = 0;
		double indice_vertice2 = 0;
		double indice_vertice3 = 0;
		
		fscanf(arquivo, "%lf", &indice_vertice1);
		fscanf(arquivo, "%lf", &indice_vertice2);
		fscanf(arquivo, "%lf", &indice_vertice3);

		Triangulo t(indice_vertice1-1, indice_vertice2-1, indice_vertice3-1);
		t.calcularBaricentro(this->lista_de_pontos_vista);
		
		this->lista_de_triangulos.push_back(t);
	}

		

	this->ordenarTriangulosBaricentro();


	/* converte os pontos para coordenada de vista
	*/
	this->mundoVista();
		/* converte os pontos para perspectiva
	*/
	this->vistaPerspectiva();
	/* converte os pontos para coordenadas de tela
	*/
	this->perspectivaTela();

	



	/* cria os vetores
	 normais dos vertices
	 e dos triangulos */
		
	this->calcularNormaisTriangulos();
	this->calcularNormaisVertices();	
	fclose(arquivo);
	

}

void Funcoes::lerParametros()
{
	FILE *arquivo = fopen("parametros.in", "r");

	char* lixo = new char[6];
//	this->objeto = new char[20];*/
	//===================================================================================
	double n1;
	double n2;
	double n3;

	fscanf(arquivo, "%s", &lixo);
	fscanf(arquivo, "%lf", &n1);
	fscanf(arquivo, "%lf", &n2);
	fscanf(arquivo, "%lf", &n3);

	Ponto pn(n1, n2, n3);
	Vetor n(pn);
	this->n = n;

	
	//===================================================================================
	double v1;
	double v2;
	double v3;

	fscanf(arquivo, "%s", &lixo);
	fscanf(arquivo, "%lf", &v1);
	fscanf(arquivo, "%lf", &v2);
	fscanf(arquivo, "%lf", &v3);

	Ponto pv(v1, v2, v3);
	Vetor v(pv);
	this->v = v;

	//===================================================================================
	
	double d;

	fscanf(arquivo, "%s", &lixo);
	fscanf(arquivo, "%lf", &d);

	this->d = d;

	//===================================================================================
	double hx;

	fscanf(arquivo, "%s", &lixo);
	fscanf(arquivo, "%lf", &hx);

	this->hx = hx;

	//===================================================================================
	
	double hy;

	fscanf(arquivo, "%s", &lixo);
	fscanf(arquivo, "%lf", &hy);

	this->hy = hy;

	//===================================================================================
	double c1;
	double c2;
	double c3;

	fscanf(arquivo, "%s", &lixo);
	fscanf(arquivo, "%lf", &c1);
	fscanf(arquivo, "%lf", &c2);
	fscanf(arquivo, "%lf", &c3);

	Ponto pc(c1, c2, c3);
	Vetor c(pc);
	this->c = c;

	//===================================================================================
	double riamb;
	double giamb;
	double biamb;

    fscanf(arquivo, "%s", &lixo);
	fscanf(arquivo, "%lf", &riamb);
	fscanf(arquivo, "%lf", &giamb);
	fscanf(arquivo, "%lf", &biamb);

	Cor iamb(riamb, giamb, biamb);
	this->iamb = iamb;

	//===================================================================================
	double ka;

	fscanf(arquivo, "%s", &lixo);
	fscanf(arquivo, "%lf", &ka);

	this->ka = ka;

	//===================================================================================
	double ril;
	double gil;
	double bil;

	fscanf(arquivo, "%s", &lixo);
	fscanf(arquivo, "%lf", &ril);
	fscanf(arquivo, "%lf", &gil);
	fscanf(arquivo, "%lf", &bil);

	Cor il(ril, gil, bil);
	this->il = il;

	//===================================================================================
	double num1;
	double num2;
	double num3;

	fscanf(arquivo, "%s", &lixo);
	fscanf(arquivo, "%lf", &num1);
	fscanf(arquivo, "%lf", &num2);
	fscanf(arquivo, "%lf", &num3);

	Ponto pl(num1, num2, num3);
	this->pl = this->pontoMundoVista(pl);
	this->pl.setX(this->pl.getX()*-1);
	this->pl.setY(this->pl.getY()*-1);
	this->pl.setZ(this->pl.getZ()*-1);
	
	
	
	//===================================================================================
	double kd1;
	double kd2;
	double kd3;

	fscanf(arquivo, "%s", &lixo);
	fscanf(arquivo, "%lf", &kd1);
	fscanf(arquivo, "%lf", &kd2);
	fscanf(arquivo, "%lf", &kd3);

	Intensidade kd(kd1, kd2, kd3);
	this->kd = kd;

	//===================================================================================
	double od1;
	double od2;
	double od3;

	fscanf(arquivo, "%s", &lixo);
	fscanf(arquivo, "%lf", &od1);
	fscanf(arquivo, "%lf", &od2);
	fscanf(arquivo, "%lf", &od3);

	Intensidade od(od1, od2, od3);
	this->od = od;

	//===================================================================================
	double ks;

	fscanf(arquivo, "%s", &lixo);
	fscanf(arquivo, "%lf", &ks);

	this->ks = ks;

	//===================================================================================
	double eta;

	fscanf(arquivo, "%s", &lixo);
	fscanf(arquivo, "%lf", &eta);

	this->eta = eta;

	//===================================================================================
	

//	fscanf(arquivo, "%s", &lixo);
//	fscanf(arquivo, "%s", this->objeto);

	
	fclose(arquivo);
}


/*
converte os pontos para os tres sistemas
*/
void Funcoes::mundoVista () {
	std::vector <Ponto>pontosVista;

	Camera *camera = new Camera();
	Vetor *base = new Vetor[3];
	base = camera->gramSchmidt(this->v,this->n);

	for(double k=0;k < this->lista_de_pontos_vista.size();k++){
		Ponto ponto =  this->lista_de_pontos_vista[k];
		Ponto vista = camera->mundiaisVista(base,this->c.getP(),ponto);
		pontosVista.push_back(vista);		
	}

this->lista_de_pontos_vista = pontosVista;

delete []base;
delete camera;
}

void Funcoes::vistaPerspectiva () {
	
	Camera *camera = new Camera();

	std::vector <Ponto> pontosPerspectiva;	
	
	for (double i = 0; i < this->lista_de_pontos_vista.size(); i++){
	
		Ponto perspec = camera->projecaoPerspectivaNormalizada(this->lista_de_pontos_vista[i], this->d, this->hx, this->hy);
		pontosPerspectiva.push_back(perspec);	

	}
	this->lista_de_pontos_tela = pontosPerspectiva;
	pontosPerspectiva.clear();
	delete camera;
}


void Funcoes::perspectivaTela () {
	
	Camera *camera = new Camera();
	
	std::vector <Ponto> pontosTela;
	double resX = 800;
	double resY = 600;
	
	for(int k = 0;k < this->lista_de_pontos_tela.size();k++){
		
		Ponto tela = camera->vistaTela(lista_de_pontos_tela[k],resX,resY);
		pontosTela.push_back(tela);
	}
	this->lista_de_pontos_tela = pontosTela;
	pontosTela.clear();
	delete camera;
}


Ponto Funcoes::pontoMundoVista (Ponto p) {
	
	Camera *camera = new Camera();
	Vetor *base = new Vetor[3];
	base = camera->gramSchmidt(this->v,this->n);
	
	Ponto vista = camera->mundiaisVista(base,this->c.getP(),p);
	delete []base;
	delete camera;
	return vista;
				
}

/*
* M�todo que varre a lista de triangulos, calcula a normal de cada triangulo
* e forma a lista de normais dos triangulos.
*/

void Funcoes::calcularNormaisTriangulos()
{
	for(double i = 0; i < this->lista_de_triangulos.size(); i++) 
	{
		Triangulo t = this->lista_de_triangulos[i];
		Vetor normal = this->calcularNormalTriangulo(t);
		this->lista_de_normais_triangulos.push_back(normal);
	}
}

/*
* M�todo que calcula a lista de normais dos vertices.
*/
void Funcoes::calcularNormaisVertices() 
{
	for(int i = 0; i < this->lista_de_triangulos.size(); i++)
	{
		
		for (int j = 0; j < 3; j++){
			
			int vertice = this->lista_de_triangulos[i].getIndice(j);
			Vetor normal_triangulo = this->lista_de_normais_triangulos[i];

			double valor_antigo1 = this->lista_de_normais_vertices[vertice].getP().getX();  
			double valor_antigo2 = this->lista_de_normais_vertices[vertice].getP().getY();
			double valor_antigo3 = this->lista_de_normais_vertices[vertice].getP().getZ();

			double valor_novo1 = valor_antigo1 + normal_triangulo.getP().getX();
			double valor_novo2 = valor_antigo2 + normal_triangulo.getP().getY();
			double valor_novo3 = valor_antigo3 + normal_triangulo.getP().getZ();

			Ponto p(valor_novo1, valor_novo2, valor_novo3);
			Vetor v1(p);
			
			this->lista_de_normais_vertices[vertice].setP(v1.getP());				
		}
		
	}
	
	for (int a = 0; a < this->lista_de_normais_vertices.size(); a++) 
	{
		this->lista_de_normais_vertices[a] = this->lista_de_normais_vertices[a].normalizar();		
	}
	
}

/*
* M�todo que calcula o vetor normal de um triangulo.
*/
Vetor Funcoes::calcularNormalTriangulo(Triangulo t) 
{
	Vetor normal;

	Ponto p1 = this->lista_de_pontos_vista[t.indice1];
	Ponto p2 = this->lista_de_pontos_vista[t.indice2];
	Ponto p3 = this->lista_de_pontos_vista[t.indice3];

	Vetor v = Vetor::diffDoisPontos(p2, p1);
	Vetor u = Vetor::diffDoisPontos(p3, p1);

	normal = v.produtoVetorial(u);
	Vetor normalizado = normal.normalizar();
	return normalizado;
}

/*
* M�todo que calcula a dist�ncia entre dois pontos.
*/
double Funcoes::distanciaDoisPontos(Ponto p1, Ponto p2) 
{
	Vetor v = Vetor::diffDoisPontos(p1, p2);
	double d = v.norma();
	return d;
}


int funcaoOrdenacaoTriangulos(const void *a, const void *b)
{
	Triangulo *t1 = new Triangulo();
	Triangulo *t2 = new Triangulo();

	*t1 = *((Triangulo *) a);
	*t2 = *((Triangulo *) b);


	double result = 0;

	if ( t1->zBaricentro < t2->zBaricentro)
	{
		result =  1;
	}
	else if ( t1->zBaricentro > t2->zBaricentro)
	{
		result =  -1;
	}
	else
	{
		result =  0;
	}

	delete t1;
	delete t2;
	return result;

}


void Funcoes::ordenarTriangulosBaricentro() {
	double qtd = this->lista_de_triangulos.size();


	Triangulo *t = new Triangulo();
	Triangulo *arrayTriangulos = (Triangulo*) new Triangulo[qtd];

	for (int i=0; i < this->lista_de_triangulos.size(); i++)
	{
		
	
	*t = lista_de_triangulos[i];
	arrayTriangulos[i] = *t;

	}
	
	qsort(arrayTriangulos, qtd, sizeof(Triangulo), &funcaoOrdenacaoTriangulos);
	std::vector <Triangulo> novo;

	for (int j = 0; j < qtd; j++) {
	novo.push_back(arrayTriangulos[j]);	
	}
	this->lista_de_triangulos = novo;
	
	delete []arrayTriangulos;
	delete t;

}



void Funcoes::scanLine (BITMAP *buffer, Triangulo* t, int indice_triangulo_vector) {


	Ponto *origem = new Ponto(0,0,0);
	Ponto *destino = new Ponto(0,0,0);

	Ponto *P1_tela = new Ponto();
	Ponto *P2_tela = new Ponto();
	Ponto *P3_tela = new Ponto();
	
	Ponto *P1_vista = new Ponto();
	Ponto *P2_vista = new Ponto();
	Ponto *P3_vista = new Ponto();
	
	Vetor *normal_vertice1 = new Vetor();
	Vetor *normal_vertice2 = new Vetor();
	Vetor *normal_vertice3 = new Vetor();
	
	*P1_tela = this->lista_de_pontos_tela[t->indice1];
	*P2_tela = this->lista_de_pontos_tela[t->indice2];
	*P3_tela = this->lista_de_pontos_tela[t->indice3];
	
	*P1_vista = this->lista_de_pontos_vista[t->indice1];
	*P2_vista = this->lista_de_pontos_vista[t->indice2];
	*P3_vista = this->lista_de_pontos_vista[t->indice3];
	
	*normal_vertice1 = this->lista_de_normais_vertices[t->indice1];
	*normal_vertice2 = this->lista_de_normais_vertices[t->indice2];
	*normal_vertice3 = this->lista_de_normais_vertices[t->indice3];

	t->ordenarPontos(P1_tela, P2_tela, P3_tela,
					P1_vista, P2_vista, P3_vista,
					normal_vertice1, normal_vertice2, normal_vertice3);

	
	//caso especial
	this->algoritmo_zBuffer(P1_tela, P1_tela, buffer, indice_triangulo_vector, t, P1_tela, P2_tela, P3_tela, P1_vista, P2_vista, P3_vista, normal_vertice1, normal_vertice2, normal_vertice3);
	

	if (P2_tela->getY() == P1_tela->getY()) {
		this->algoritmo_zBuffer(P1_tela, P2_tela, buffer, indice_triangulo_vector, t, P1_tela, P2_tela, P3_tela, P1_vista, P2_vista, P3_vista, normal_vertice1, normal_vertice2, normal_vertice3);
	} else {
		for (int a = 1; a <= P2_tela->getY() - P1_tela->getY(); a ++){
			if (a == P2_tela->getY() - P1_tela->getY()) {
				this->algoritmo_zBuffer(P1_tela, P2_tela, buffer, indice_triangulo_vector, t, P1_tela, P2_tela, P3_tela, P1_vista, P2_vista, P3_vista, normal_vertice1, normal_vertice2, normal_vertice3);
			} else {	
				P1_tela->calcula_novo_ponto(origem, P2_tela,a); 
				P1_tela->calcula_novo_ponto(destino,P3_tela,a);
				this->algoritmo_zBuffer(origem, destino, buffer, indice_triangulo_vector, t, P1_tela, P2_tela, P3_tela, P1_vista, P2_vista, P3_vista, normal_vertice1, normal_vertice2, normal_vertice3);
			}
		}
	}	

	if (P2_tela->getY() > P1_tela->getY()){		
	
		P1_tela->calcula_novo_ponto(origem, P2_tela, P2_tela->getY() - P1_tela->getY()); 
		P1_tela->calcula_novo_ponto(destino, P3_tela, P2_tela->getY() - P1_tela->getY());
		this->algoritmo_zBuffer(origem, destino, buffer, indice_triangulo_vector, t, P1_tela, P2_tela, P3_tela, P1_vista, P2_vista, P3_vista, normal_vertice1, normal_vertice2, normal_vertice3);
	}


	int b = 1;
	if (P3_tela->getY() == P2_tela->getY()) {
	this->algoritmo_zBuffer(P2_tela, P3_tela, buffer, indice_triangulo_vector, t, P1_tela, P2_tela, P3_tela, P1_vista, P2_vista, P3_vista, normal_vertice1, normal_vertice2, normal_vertice3);
		
	} else {
		for (int a = P2_tela->getY() - P1_tela->getY()+1; b <= P3_tela->getY() - P2_tela->getY(); a ++){			
			if (b == P3_tela->getY() - P2_tela->getY()){
			this->algoritmo_zBuffer(P3_tela, P2_tela, buffer, indice_triangulo_vector, t, P1_tela, P2_tela, P3_tela, P1_vista, P2_vista, P3_vista, normal_vertice1, normal_vertice2, normal_vertice3);
			} else {
			P2_tela->calcula_novo_ponto(origem, P3_tela,b); 
			P1_tela->calcula_novo_ponto(destino, P3_tela,a);			
			this->algoritmo_zBuffer(origem, destino, buffer, indice_triangulo_vector, t, P1_tela, P2_tela, P3_tela, P1_vista, P2_vista, P3_vista, normal_vertice1, normal_vertice2, normal_vertice3);
			}
		b++;
		}
	}	
	

	delete origem;
	delete destino;
	delete P1_tela;
	delete P2_tela;
	delete P3_tela;	
	delete P1_vista;
	delete P2_vista;
	delete P3_vista;	
	delete normal_vertice1;
	delete normal_vertice2;
	delete normal_vertice3;
}

//drawline implementado mesclado com o zbuffer

void Funcoes::algoritmo_zBuffer(Ponto* Origem, Ponto* Destino, BITMAP *buffer, 
								int indice_triangulo_vector, Triangulo* triangulo,
								Ponto* P1_tela, Ponto* P2_tela,	Ponto* P3_tela,
								Ponto* P1_vista, Ponto* P2_vista, Ponto* P3_vista,
								Vetor* normal_vertice1,	Vetor* normal_vertice2,
								Vetor *normal_vertice3) {


	double dx = (Destino->getX() - Origem->getX());
    double dy = (Destino->getY() - Origem->getY());
    int x1,x2,y1,y2;
    x1 = Origem->getX();
    y1 = Origem->getY();
    x2 = Destino->getX();
    y2 = Destino->getY();
	float t = 0.5;		


	this->putPixel(x1, y1, triangulo, buffer, P1_tela, P2_tela, P3_tela, 
					P1_vista, P2_vista, P3_vista, normal_vertice1,
					normal_vertice2, normal_vertice3);
	
	if (abs(dx) > abs(dy)) {
	    float a = (float) dy / (float) dx;
		t = t + y1;
		dx = (dx < 0) ? -1 : 1;
		a = a * dx;
             
		while (x1 != x2) {
			x1 = (x1 + dx);
			t = t + a;
			this->putPixel(x1, t, triangulo, buffer, P1_tela, P2_tela, P3_tela, 
					P1_vista, P2_vista, P3_vista, normal_vertice1,
					normal_vertice2, normal_vertice3);
   	   	}
	
	} else {
		if ( dy != 0 ) {
			float a = (float) dx / (float) dy;
			t = t + x1;
			dy = (dy < 0) ? -1 : 1;
			a = a * dy;
					
			while ( y1 != y2) {
				y1 = (y1 + dy);
				t = t + a;
				this->putPixel(t, y1, triangulo, buffer, P1_tela, P2_tela, P3_tela, 
					P1_vista, P2_vista, P3_vista, normal_vertice1,
					normal_vertice2, normal_vertice3);
			 }
		}
	}	
}

void Funcoes::putPixel (int x, int y, Triangulo* triangulo, BITMAP* buffer, Ponto* P1_tela,
	Ponto* P2_tela, Ponto* P3_tela,	Ponto* P1_vista, Ponto* P2_vista, Ponto* P3_vista, 
	Vetor* normal_vertice1, Vetor* normal_vertice2, Vetor* normal_vertice3) {


	
	double profundidade_z = 0;
	double valor_matriz_z = 0;
	Cor *c =  new Cor();
	Ponto *p = new Ponto();
	Vetor *n = new Vetor();
	
	double *alfa_beta_gama = new double[3];
	alfa_beta_gama = triangulo->calcularAlfaBetaGama(x,y,*P1_tela,*P2_tela,*P3_tela);		

		if(alfa_beta_gama != NULL){

			*p = triangulo->coordZbaricentro(alfa_beta_gama,*P1_vista,*P2_vista,*P3_vista);
			profundidade_z = p->getZ();
			
			if (x >= 0 && y >= 0 && x <= 799 && y <= 599) {

				valor_matriz_z = this->zBuffer[(int)x][(int)y];

				if (profundidade_z < valor_matriz_z && profundidade_z > this->d) {	

					*n = normal_vertice1->escalar_x_Vetor(alfa_beta_gama[0]) +
						normal_vertice2->escalar_x_Vetor(alfa_beta_gama[1]) + 
						normal_vertice3->escalar_x_Vetor(alfa_beta_gama[2]);

					*n = n->normalizar();
					*c = this->calcularCor(*p, *n, *triangulo);
					putpixel(buffer, (int)x, (int)y, makecol(c->getR(),c->getG(),c->getB()));
					this->zBuffer[(int)x][(int)y] = profundidade_z;
				}
			}
		}
		
		
		delete []alfa_beta_gama;
		delete c;
		delete p;
		delete n;		
}



//=========================================================================
//PHONG
Cor Funcoes::calcCompAmbiental()
{
	double iar = this->ka * this->iamb.getR();
	double iag = this->ka * this->iamb.getG();
	double iab = this->ka * this->iamb.getB();

	Cor ia(iar, iag, iab);
	return ia;
}

Intensidade Funcoes::calcCompDifusa(Ponto pVarrido, Vetor n) 
{
	
	//TODO

	Intensidade id(idr, idg, idb);
	return id;
}

Intensidade Funcoes::calcCompEspecular(Ponto pVarrido, Vetor n) 
{	
	Vetor vpl(this->pl);
	Vetor vpVarrido(pVarrido);
	
	//TODO

	Intensidade is(isr, isg, isb);
	return is;
}

Cor Funcoes::calcularCor(Ponto pVarrido, Vetor n, Triangulo t) {
	
	//calcular n vezes l
	Vetor vpl(this->pl);
	Vetor vpVarrido(pVarrido);
	
	Vetor v = vpVarrido.escalar_x_Vetor(-1);
	v = v.normalizar();
	

	Vetor l = vpl - vpVarrido;

	Vetor lNormalizado = l.normalizar();

	double nVezesL = n.produtoEscalar(lNormalizado);

	double r;
	double g;
	double b;
	
	if (nVezesL < 0) {
		double vVezesN = v.produtoEscalar(n);

		if (vVezesN < 0) {
			Vetor nInvertido = n.escalar_x_Vetor(-1);
		
			Cor ia = this->calcCompAmbiental();
			Intensidade id = this->calcCompDifusa(pVarrido,nInvertido);
			Intensidade is = this->calcCompEspecular(pVarrido, nInvertido);

			r = (ia.getR() + id.getIR() + is.getIR());
			g = (ia.getG() + id.getIG() + is.getIG());
			b = (ia.getB() + id.getIB() + is.getIB());

		} else {
			Cor ia = this->calcCompAmbiental();

			r = (ia.getR());
			g = (ia.getG());
			b = (ia.getB());
		}


	}
	else {
		double vVezesN = v.produtoEscalar(n);
		if(vVezesN > 0){
			Cor ia = this->calcCompAmbiental();
			Intensidade id = this->calcCompDifusa(pVarrido,n);
			Intensidade is = this->calcCompEspecular(pVarrido,n);

			r = (ia.getR() + id.getIR() + is.getIR());
			g = (ia.getG() + id.getIG() + is.getIG());
			b = (ia.getB() + id.getIB() + is.getIB());
		}else{
			Cor ia = this->calcCompAmbiental();

			r = (ia.getR());
			g = (ia.getG());
			b = (ia.getB());
		}
	}


	if (r < 0) {
		r = 0;
	}
	if (g < 0) {
		g = 0;
	}
	if (b < 0) {
		b = 0;
	}
	if (r > 255) {
		r = 255;
	}
	if (g > 255) {
		g = 255;
	}
	if (b > 255) {
		b = 255;
	}

	Cor c(r, g, b);
	return c;
}


void Funcoes::imprimirTriangulos(BITMAP* buffer){

	for (int i = 0;i < this->lista_de_triangulos.size(); i++) {	

		this->scanLine(buffer, &this->lista_de_triangulos[i], i);
		blit (buffer, screen, 0, 0, 0, 0, 800,600);	 	
	}
}

Funcoes::~Funcoes () {

	for (int a = 0; a < 800; a ++)	{		
		
		delete []this->zBuffer[a];
	}
	
	delete []this->zBuffer;
	this->lista_de_triangulos.clear();
	this->lista_de_normais_triangulos.clear();
	this->lista_de_normais_vertices.clear();
	this->lista_de_pontos_tela.clear();
	this->lista_de_pontos_vista.clear();
}






//=========================================================================